//
//  AppDelegate.h
//  EjemploGestos
//
//  Created by José Manuel Fierro Conchouso on 14/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
