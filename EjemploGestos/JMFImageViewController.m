//
//  JMFImageViewController.m
//  EjemploGestos
//
//  Created by José Manuel Fierro Conchouso on 14/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFImageViewController.h"

@interface JMFImageViewController ()

@property (nonatomic) CGPoint initialTouch;

@end

@implementation JMFImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    
//    NSData *imageData = [NSData dataWithContentsOfFile:@"Gaviota.png"];
//    UIImage *imageUI = [UIImage imageWithData:imageData];
//
//    self.image. = imageUI;

//      UIImage *imageUI = [UIImage imageNamed:@"Gaviota.png"];
//    
//      self.shiplfImage = imageUI;
//

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pan:(id)sender {
    

    
    // Para estar seguro de que es de UIPanGestureRecognizer
//    UIPanGestureRecognizer *panRec = (UIPanGestureRecognizer*)sender;
//    if (panRec.state == UIGestureRecognizerStateBegan) {
//      self.initialTouch = [panRec locationInView:self.view];  //tapRec.view]; (Referencia view)
//
////        self.initialTouch = [panRec locationInView:self.ship];
//        
//        CGPoint currentTouch = [panRec locationInView:self.view];
//        CGFloat newX = currentTouch.x - self.initialTouch.x;
//        CGFloat newY = currentTouch.y - self.initialTouch.y;
//        
//        CGPoint newCenter = CGPointMake(newX + (self.ship.frame.size.width / 2), newY + (self.ship.frame.size.height / 2));
//        
//        self.ship.center = newCenter;
//        
//        
//        panRec.view.center = CGPointMake(panRec.view.center.x + translation.x,
//                                         panRec.view.center.y + translation.y);
//        [panRec setTranslation:CGPointMake(0, 0) inView:self.view];
//                                              
//        self.ship.center = translation;
    
    UIPanGestureRecognizer *panRec = (UIPanGestureRecognizer*)sender;
    if (panRec.state == UIGestureRecognizerStateBegan) {
        self.initialTouch = [panRec locationInView:self.ship];
    }
    CGPoint currentTouch = [panRec locationInView:self.view];
    CGFloat newX = currentTouch.x - self.initialTouch.x;
    CGFloat newY = currentTouch.y - self.initialTouch.y;
    CGPoint newCenter = CGPointMake(newX + (self.ship.frame.size.width / 2), newY + (self.ship.frame.size.height / 2));
    self.ship.center = newCenter;
    

    
        

    
//    self.ship.center = [panRec locationInView:self.view];
    
    // (Referencia imagen fondo)
//    CGPoint translation = [panRec locationInView:panRec.view];  //tapRec.view]; (Referencia view)
//    panRec.view.center = CGPointMake(panRec.view.center.x + translation.x,
//                                         panRec.view.center.y + translation.y);
//    [panRec setTranslation:CGPointMake(0, 0) inView:self.view];
    
//    self.ship.center = translation;
}

- (IBAction)swipe:(id)sender {
    
//    UISwipeGestureRecognizer *swipeGes = (UISwipeGestureRecognizer*)sender;
    UIPanGestureRecognizer *panRec = (UIPanGestureRecognizer*)sender;
    if (panRec.state == UIGestureRecognizerStateRecognized) {
        
        CGPoint touchPoint = [panRec locationInView:self.view];
        
        // Evita problemas de memoria
        __block JMFImageViewController *weakSelf = self;
        [UIView animateWithDuration:1.0f animations:^{
            weakSelf.ship.alpha = 0.0;
        
        
        }completion:^(BOOL finished) {
            weakSelf.ship.center = touchPoint;
            [UIView animateWithDuration:1.0f animations:^{
                weakSelf.ship.alpha = 1.0;
        
            }];
        }];
    }
    
}




- (IBAction)tap:(id)sender {
    
    // Para estar seguro de que es de UITapGestureRecognizer
    UITapGestureRecognizer *tapRec = (UITapGestureRecognizer*)sender;
    
    // (Referencia imagen fondo)
    CGPoint touchPoint = [tapRec locationInView:self.view];  //tapRec.view]; (Referencia view)
//    NSLog(@"(%.2f, %.2f)", touchPoint.x, touchPoint.y);
    
//    [UIView animateWithDuration:3.0f animations:^{
//        self.ship.center = touchPoint;
//    }];
    
//
    
   
//    [UIView animateWithDuration:2.0f delay:0.0f options:UIViewAnimationOptionBeginFromCurrentState animations:^{self.ship.center = touchPoint;} completion:nil];

    
    //    [UIView animateWithDuration:2.0f
    //                          delay:0.0f
    //                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn
    //                     animations:^{
    //                                                  self.ship.center = touchPoint;
    //                                                  self.ship.transform = CGAffineTransformMakeRotation(M_PI_4);
    //
    //
    //
    //                         [UIView animateWithDuration:0.0f animations:^{
    //                             self.ship.transform = CGAffineTransformIdentity;
    //                         }];
    //                     }
    //                     completion:^(BOOL finished) {
    //                         [UIView animateWithDuration:3.0f animations:^{
    //                             self.ship.transform = CGAffineTransformIdentity;
    //                         }];
    //                         
    //                     }];
    


    
    
    [UIView animateWithDuration:2.0f
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseIn
                     animations:^{
//                         self.ship.center = touchPoint;
//                         self.ship.transform = CGAffineTransformMakeRotation(M_PI_4);
                         
                         CGPoint touchMinddle;
                         touchMinddle.x = self.view.center.x + ((touchPoint.x - self.view.center.x)/3);
                         touchMinddle.y = self.view.center.y + ((touchPoint.y - self.view.center.y)/3);
                         
                         NSLog(@"(self.view.center.x: %.2f, %.2f)", self.view.center.x,self.view.center.y);
                         NSLog(@"(touchPoint: %.2f, %.2f)", touchPoint.x, touchPoint.y);
                         NSLog(@"(touchMinddle: %.2f, %.2f)", touchMinddle.x, touchMinddle.y);
                         
                         
                         self.ship.center = touchMinddle;
                         self.ship.transform = CGAffineTransformMakeRotation(M_PI_4);
                  
                     }
                          completion:^(BOOL finished) {
                              [UIView animateWithDuration:2.0f animations:^{
                                   self.ship.center = touchPoint;
                                  self.ship.transform = CGAffineTransformIdentity;
                              }];
     
                          }];
 
     /**
      * Solución del profesor
      */
    
     // Animación para el desplazamiento
//    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut;
//    [UIView animateWithDuration:2 delay:0 options:options animations:^{
//        self.ship.center = touchPoint;
//    } completion:nil];
    
//    // Animación para la rotación
//    [UIView animateWithDuration:1.0 delay:0 options:options animations:^{
//        self.ship.transform = CGAffineTransformMakeRotation(M_PI_4);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.75 delay:0 options:options animations:^{
//            self.ship.transform = CGAffineTransformIdentity;
//        } completion:nil];
//    }];
    

   
    
}


@end
