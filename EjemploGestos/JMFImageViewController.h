//
//  JMFImageViewController.h
//  EjemploGestos
//
//  Created by José Manuel Fierro Conchouso on 14/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFImageViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *image;
@property (weak, nonatomic) IBOutlet UIImageView *ship;
- (IBAction)pan:(id)sender;
- (IBAction)swipe:(id)sender;


- (IBAction)tap:(id)sender;



@end
